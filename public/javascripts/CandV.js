'use strict';
export const alfa = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';           //define chars

const xtndKey = function (text, key) {
    let s = '';
    for (let i = 0; i < text.length; i++) {
        s += key.charAt(i % key.length);                    // change to map
    }
    s = s.toUpperCase();
    console.log(`key: ${s}`);
    return s;
}

export class CandV {
    constructor(alphanum) {
        this.alphabet = alphanum;
        this.cpad = new Map();
        this.vpad = new Map();
        this.toPad(this.cpad, 'digi');
        this.toPad(this.vpad, alfa);
    }

    caesarE(plaintext, key) {
        let c = plaintext.toUpperCase();                        //convert all to upper
        let s = '';
        let i = 0;
        let normkey = (this.alphabet.length + key) % this.alphabet.length; //normalize key
        console.log(`encrypt key: ${normkey}`);
        while (i < c.length) {                                  //loop through cleartext
            let input = this.cpad.get(0);                       //input is top row alphabet
            let index = input.indexOf(c.charAt(i));             //get index of letter
            let alfai = input.charAt(index);                    //get input letter
            let alfao = this.cpad.get(normkey).charAt(index);   //from output alphabet, key offset row
            s += alfao;
            i++;
        }
        return s;
    }
    caesarD(plaintext, key) {
        let c = plaintext.toUpperCase();                    //convert all to upper
        let s = '';
        let i = 0;
        let normkey = (this.alphabet.length + key) % this.alphabet.length; //normalize key
        console.log(`decrypt key: ${normkey}`);
        while (i < c.length) {                              //loop through cleartext
            let input = this.cpad.get(normkey);                       //input is alphabet at offset key
            let index = input.indexOf(c.charAt(i));             //get index of letter
            let alfai = input.charAt(index);                    //get input letter
            let alfao = this.cpad.get(0).charAt(index);   //from output alphabet, key offset row
            s += alfao;
            i++;
        }
        return s;
    }

    vigE(plaintext, key) {
        let k = xtndKey(plaintext, key);
        let c = plaintext.toUpperCase();                            //convert all to upper
        let s = '';
        let i = 0;

        while (i < c.length) {
            let input = this.vpad.get(this.alphabet.charAt(0));     // input alpha
            let index= input.indexOf(c.charAt(i));                  // index of char[i] in input alpha

            let alfai = input.charAt(index);                        // input char
            let alfao = this.vpad.get(k.charAt(i)).charAt(index);   // corresponding char from output alphabet
            s += alfao;
            i++;
        }
        return s;
    }
    vigD(plaintext, key) {
        let k = xtndKey(plaintext, key);
        let c = plaintext.toUpperCase();                        //convert all to upper
        let s = '';
        let i = 0;
        while (i < c.length) {                                  //loop through cleartext
            let input = this.vpad.get(k.charAt(i));     // input alpha
            let index= input.indexOf(c.charAt(i));                  // index of char[i] in input alpha

            let alfai = input.charAt(index);                        // input char
            let alfao = this.vpad.get(this.alphabet.charAt(0)).charAt(index);   // corresponding char from output alphabet
            s += alfao;
            i++;
        }
        return s;
    }

    toPad(pad, key) {
        for (let i = 0; i < this.alphabet.length; i++) {
            let s = '';
            let k = i;
            for (let j = 0; j < this.alphabet.length; j++) {
                s += this.alphabet.charAt(k);
                k = ++k % this.alphabet.length;
            }
            if (key === 'digi')
                pad.set(i, s);
            else
                pad.set(this.alphabet.charAt(i), s);
        };
    }

    toString(pad) {
        let s = `<table class='skema'>`;
        s += '<tr>';
        for (let [key, monoAlpha] of pad) {
            if (key === 0 || key === this.alphabet.charAt(0)) {
                s += '<thead>';
                s += '<tr>';
                s += `<th class='col0 row0'> Keys: ${key}</th>`;
                for (let j = 0; j < monoAlpha.length; j++) {
                    s += `<th class='row0'>${monoAlpha.charAt(j % monoAlpha.length)}</th>`;
                }
                s += '</tr>';
                s += '</thead>';
            } else {
                s += '<tr>';
                s += `<th class='col0'>${key}</th>`;
                for (let j = 0; j < monoAlpha.length; j++) {
                    s += `<td>${monoAlpha.charAt(j)}</td>`;
                }
                s += '</tr>';
            }
        };
        s += '</tbody>';
        s += '</table>';
        return s;
    }
}