'use strict';
import {$, makeMenu, setTitles, setFooter} from '/javascripts/nQm.js';
import {CandV, alfa} from '/javascripts/CandV.js';

const doTheStart = function () {
    makeMenu();
    setTitles();
    setFooter('nml', 2022);
    let t = new CandV(alfa);

    if ($('skema')) {
        $('skema').innerHTML = t.toString(t.cpad);
    }
    if ($('encrypt')) {
        $('encrypt').addEventListener('click', function () {
            let key = parseInt($('f13').key.value);         //get offset key
            let c = $('f13').plaintext.value;               //get plaintext
            $('f13').ciphertext.value = t.caesarE(c, key);
            $('f13').plaintext.value = '';
        });
    }
    if ($('decrypt')) {
        $('decrypt').addEventListener('click', function () {
            let key = parseInt($('f13').key.value);         //get offset key
            let c = $('f13').ciphertext.value;              //get ciphertext
            $('f13').plaintext.value = t.caesarD(c, key);
            $('f13').ciphertext.value = '';
        });
    }
}
window.addEventListener('load', doTheStart);