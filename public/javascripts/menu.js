'use strict';
const root = '/';

export const title = 'Basic Ciphers';
export const menu = [
    {
        text: "Home",
        url: root+"index.html"
    },
    {
        text: "Caesar",
        url: root+"pages/caesar0.html"
    },
    {
        text: "Vigenére",
        url: root+"pages/vigenere0.html"
    },
    {
        text: "TextAnalysis",
        url: root+"pages/textAnalysis.html"
    }
];