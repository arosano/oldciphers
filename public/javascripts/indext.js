'use strict';

import {$, makeMenu, setTitles, setFooter} from './nQm.js';
import {TextAnalysis, Danish} from './TextAnalysis.js';

const createLetterFreq = function (foo) {
    let o = {};
    const pr = function (count, item, foo) {
        o[item] = count;
    };
    foo.forEach(pr);
    return o;
};

const anal = function (e) {
    $('letters').innerHTML = '';
    let s = $('poe').value;
    let analobj = new TextAnalysis(s);
    analobj.createLetterMap();

    $('letters').innerHTML = analobj.lf2Table();
    let danish = new Danish();
    $('language').innerHTML = danish.lf2Table();
};

const go = function () {
    makeMenu('menu');
    setTitles('Analyze Text');
    setFooter('nml', 2021);
    if ($('b1'))
        $('b1').addEventListener('click', anal);
};

window.addEventListener('load', go);  // kick off JS
// http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/substitution.html
