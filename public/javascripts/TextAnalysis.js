export class Danish {
    static Danish_lf = new Map(
        Object.entries({
            'e': 16.6,
            'r': 8.0,
            'n': 7.7,
            't': 7.2,
            'd': 6.7,
            'i': 5.8,
            'a': 5.6,
            's': 5.5,
            'l': 5.1,
            'o': 4.5,
            'g': 4.4,
            'm': 3.7,
            'k': 3.2,
            'v': 2.7,
            'f': 2.6,
            'h': 2.1,
            'u': 1.6,
            'b': 1.4,
            'p': 1.3,
            'å': 1.3,
            'ø': 0.9,
            'æ': 0.8,
            'y': 0.6,
            'j': 0.6,
            'c': 0.1,
        })
    )
    /*
     * toString of language letter frequencies
     */
    lf2Table() {
        let s = '<table>';
        for (let [key, value] of Danish.Danish_lf) {
            s += `<tr><th>${key}</th><td class='right'>${value}</td></tr>`;
        }
        s += '</table>';
        return s;
    }
}

export class TextAnalysis {
    static REGEX = /\b[a-zæøå]+\b/gi;
    static REGEXC = /[a-zæøå]/gi;
    static REGEXL = /[a-zæøå]/;

    constructor(str) {
        this.str = str;
        this.wl = new Map();
        this.lf = new Map();
        this.wc = 0;
        this.lc = 0;
    }

    /*
     * Cretes total word count, and
     * maps of words and their occurrences
     */
    createWordList() {
        let foo = this.str.match(TextAnalysis.REGEX);
        this.wc = foo.length
        for (let word of foo) {
            word = word.toLowerCase();
            let c = 1;
            if (this.wl.get(word)) {
                c = this.wl.get(word);
                c++;
            }
            this.wl.set(word, c);
        }
        this.wl = new Map([...this.wl.entries()].sort());
    }
    /*
     *  Getter total word count
     */
    getWordCount() {
        return this.wc;
    }
    /*
     * Getter different words
     */
    getDiffWords() {
        return this.wl.size;
    }
    /*
     * Creates map of letters and their counts
     */
    createLetterMap() {
        let foo = this.str.match(TextAnalysis.REGEXC);
        for (let i = 0; i < foo.length; i++) {
            let letter = foo[i].toLowerCase();

            if (! TextAnalysis.REGEXL.test(letter)) continue;
            this.lc++;
            let c = 1;
            if (this.lf.get(letter)) {
                c = this.lf.get(letter);
                c++;
            }
            this.lf.set(letter, c);
        }
        this.lf = new Map([...this.lf.entries()].sort(function (a, b) {
            return b[1] - a[1];         // sort by value, desc
        }));
    }
    /*
     * toString of word list
     */
    wl2Table() {
        let s = '<table><tbody>';
        for (let [key, value] of this.wl) {
            s += `<tr><td>${key}</td><td class='right'>${value}</td></tr>`;
        }
        s += '</tbody></table>';
        return s;
    }
    /*
     * toString of letter frequencies
     */
    lf2Table() {
        let s = '<table>';
        for (let [key, value] of this.lf) {
            let p = (value * 100 / this.lc).toFixed(2);
            s += `<tr><th>${key}</th><td class='right'>${value}</td><td class='right'>${p}%</td></tr>`;
        }
        s += '</table>';
        return s;
    }
    /*
     * toString of letter frequencies as histogram
     */
    lf2Histo() {
        let scale = 3.5;
        let s = '';
        for (let [key, value] of this.lf) {
            let p = (value * 100 / this.lc).toFixed(1);
            s += `${key}: |`;
            for (let i = p * scale; i > 0; i--)
                s += '*';
            s += '<br/>';
        }
        return s;
    }
}
